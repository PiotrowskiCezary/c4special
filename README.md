![Logo](Docs/logo.png)

# C4Special

[![Version](https://img.shields.io/badge/Version-1.0-blue.svg?style=flat-square)](https://bitbucket.org/PiotrowskiCezary/c4special/downloads/C4Special.zip)
[![Unity version](https://img.shields.io/badge/Unity-2020.3%20(LTS)-lightgray.svg?style=flat-square&logo=unity)](https://unity3d.com/unity/qa/lts-releases?version=2020.3)
[![ML-Agents release](https://img.shields.io/badge/ML--Agents-Release%2018-success.svg?style=flat-square&logo=github)](https://github.com/Unity-Technologies/ml-agents/tree/release_18)

---

**C4Special** is a Connect 4 type game featuring AI agents trained via self-play, developed in Unity.

---

## Download

- [ZIP file](https://bitbucket.org/PiotrowskiCezary/c4special/downloads/C4Special.zip) (SHA-256: d4d6a92eb45c55006b21fc653c0dc76c0ce1fc9d55340e2594feb48d046b23ce)

---

## Installation

### Minimum requirements

- **Operating System:** Windows 7 (SP1+) or later
- **CPU:** 32-bit or 64-bit
- **Graphics card:** DirectX 10 capable or later
- **Storage:** 70 MB available space

---

## Technologies used

- Unity 2020.3.23f1
- Unity ML-Agents Release 18
- Python 3.7.9
- PyTorch 1.7.1

---

## About

### 4 different modes

There are 4 different modes to choose for each side:

- **Random:** chooses a random move in each of its turns
- **Human:** play via mouse and/or keyboard
- **MinMax:** utilizes a tree search algorithm to find the optimal move (8 levels of difficulty)
- **AI:** uses a pre-trained neural network to make a move (3 levels of difficulty)

![Different modes](Docs/showcase1.gif)

### Multiple ways to play

Play singeplayer, hotseat or just watch the computer play.

![AI gameplay](Docs/showcase2.gif)

### Controls

- **Mouse:** Left mouse button
- **Keyboard:** 1-7 key (corresponds to the 1st-7th column)

![Human vs MinMax gameplay](Docs/showcase3.gif)

---

## Example configuration file

If you want to train your own agent you can use (or tweak) the settings below.

This is the ***Medium agent*** training configuration file:

**config.yaml**

```yaml
behaviors:
  C4Special:
    trainer_type: sac
    hyperparameters:
      learning_rate: 0.0003
      learning_rate_schedule: constant
      batch_size: 128
      buffer_size: 50000
      buffer_init_steps: 1000
      tau: 0.005
      steps_per_update: 10.0
      save_replay_buffer: false
      init_entcoef: 0.5
      reward_signal_steps_per_update: 10.0
    network_settings:
      normalize: false
      hidden_units: 256
      num_layers: 5
      vis_encode_type: simple
    reward_signals:
      extrinsic:
        gamma: 0.9
        strength: 1.0
    keep_checkpoints: 20
    checkpoint_interval: 20000
    max_steps: 400000
    time_horizon: 84
    summary_freq: 5000
    threaded: false
    self_play:
      save_steps: 10000
      team_change: 20000
      swap_steps: 200
      window: 20
      play_against_latest_model_ratio: 0.2
      initial_elo: 1200.0
```
