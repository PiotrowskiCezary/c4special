using System.Collections.Generic;
using UnityEngine;

public class MinMax : MonoBehaviour
{
    public BoardManager boardManager;
    public int[] window = new int[Constants.DISCS_TO_WIN];

    public int[] Algorithm(int[,] board, int depth, int alpha, int beta, int player, bool maximizing)
    {
        int winner = (maximizing) ? player : -player;
        List<int> validMoves = GetValidMoves(board);

        if (IsWinning(board, winner))
        {
            return new int[2] { Constants.MOVE_INVALID, int.MaxValue };
        }
        else if (IsWinning(board, -winner))
        {
            return new int[2] { Constants.MOVE_INVALID, int.MinValue };
        }
        else if (validMoves.Count == 0)
        {
            return new int[2] { Constants.MOVE_INVALID, 0 };
        }
        else if (depth == 0)
        {
            return new int[2] { Constants.MOVE_INVALID, Score(board, winner) };
        }

        int column = ChooseRandom(validMoves);

        if (maximizing)
        {
            int value = int.MinValue;
            for (int i = 0; i < validMoves.Count; i++)
            {
                int y = MoveCheck(board, validMoves[i]);
                int[,] mockBoard = (int[,])board.Clone();
                mockBoard[validMoves[i], y] = player;
                int newScore = Algorithm(mockBoard, depth - 1, alpha, beta, -player, false)[1];
                if (newScore > value)
                {
                    value = newScore;
                    column = validMoves[i];
                }
                alpha = (alpha > value) ? alpha : value;
                if (alpha >= beta)
                {
                    break;
                }
            }

            return new int[2] { column, value };
        }
        else
        {
            int value = int.MaxValue;
            for (int i = 0; i < validMoves.Count; i++)
            {
                int y = MoveCheck(board, validMoves[i]);
                int[,] mockBoard = (int[,])board.Clone();
                mockBoard[validMoves[i], y] = player;
                int newScore = Algorithm(mockBoard, depth - 1, alpha, beta, -player, true)[1];
                if (newScore < value)
                {
                    value = newScore;
                    column = validMoves[i];
                }
                beta = (beta < value) ? beta : value;
                if (alpha >= beta)
                {
                    break;
                }
            }

            return new int[2] { column, value };
        }
    }

    bool IsWinning(int[,] board, int player)
    {
        return CheckHorizontally(ref board, ref player)
            || CheckVertically(ref board, ref player)
            || CheckDiagonally(ref board, ref player);
    }

    bool CheckHorizontally(ref int[,] board, ref int player)
    {
        for (int j = 0; j < Constants.Y; j++)
        {
            for (int i = 0; i < Constants.X - (Constants.DISCS_TO_WIN - 1); i++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.HORIZONTAL);
                if (CheckWin(ref player))
                {
                    return true;
                }
            }
        }

        return false;
    }

    bool CheckVertically(ref int[,] board, ref int player)
    {
        for (int i = 0; i < Constants.X; i++)
        {
            for (int j = 0; j < Constants.Y - (Constants.DISCS_TO_WIN - 1); j++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.VERTICAL);
                if (CheckWin(ref player))
                {
                    return true;
                }
            }
        }

        return false;
    }

    bool CheckDiagonally(ref int[,] board, ref int player)
    {
        for (int j = 0; j < Constants.Y - (Constants.DISCS_TO_WIN - 1); j++)
        {
            for (int i = 0; i < Constants.X - (Constants.DISCS_TO_WIN - 1); i++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.DIAGONAL_UP);
                if (CheckWin(ref player))
                {
                    return true;
                }
                GetWindowValues(ref i, ref j, ref board, Constants.DIAGONAL_DOWN);
                if (CheckWin(ref player))
                {
                    return true;
                }
            }
        }

        return false;
    }
    
    bool CheckWin(ref int player)
    {
        if (Count(ref window, player) == Constants.DISCS_TO_WIN)
        {
            return true;
        }

        return false;
    }

    List<int> GetValidMoves(int[,] board)
    {
        List<int> moves = new List<int>();

        for (int i = 0; i < Constants.X; i++)
        {
            if (board[i, Constants.Y - 1] == 0)
            {
                moves.Add(i);
            }
        }

        return moves;
    }
    
    int Score(int[,] board, int player)
    {
        int score = 0;
        score += ScoreHorizontally(ref board, ref player);
        score += ScoreVertically(ref board, ref player);
        score += ScoreDiagonally(ref board, ref player);
        score += ScoreCenter(ref board, ref player);
        return score;
    }

    int ScoreHorizontally(ref int[,] board, ref int player)
    {
        int score = 0;

        for (int j = 0; j < Constants.Y; j++)
        {
            for (int i = 0; i < Constants.X - (Constants.DISCS_TO_WIN - 1); i++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.HORIZONTAL);
                score += CalculateScore(ref player);
            }
        }

        return score;
    }

    int ScoreVertically(ref int[,] board, ref int player)
    {
        int score = 0;

        for (int i = 0; i < Constants.X; i++)
        {
            for (int j = 0; j < Constants.Y - (Constants.DISCS_TO_WIN - 1); j++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.VERTICAL);
                score += CalculateScore(ref player);
            }
        }

        return score;
    }

    int ScoreDiagonally(ref int[,] board, ref int player)
    {
        int score = 0;

        for (int j = 0; j < Constants.Y - (Constants.DISCS_TO_WIN - 1); j++)
        {
            for (int i = 0; i < Constants.X - (Constants.DISCS_TO_WIN - 1); i++)
            {
                GetWindowValues(ref i, ref j, ref board, Constants.DIAGONAL_UP);
                score += CalculateScore(ref player);
                GetWindowValues(ref i, ref j, ref board, Constants.DIAGONAL_DOWN);
                score += CalculateScore(ref player);
            }
        }

        return score;
    }

    int ScoreCenter(ref int[,] board, ref int player)
    {
        int score = 0;

        for (int i = 0; i < Constants.Y; i++)
        {
            if (board[Constants.X / 2, i] == player)
            {
                score += Constants.DISCS_CENTER;
            }
        }

        return score;
    }

    void GetWindowValues(ref int x, ref int y, ref int[,] board, int direction)
    {
        for (int i = 0; i < Constants.DISCS_TO_WIN; i++)
        {
            if (direction == Constants.HORIZONTAL)
            {
                window[i] = board[x + i, y];
            }
            else if (direction == Constants.VERTICAL)
            {
                window[i] = board[x, y + i];
            }
            else if (direction == Constants.DIAGONAL_UP)
            {
                window[i] = board[x + i, y + i];
            }
            else if (direction == Constants.DIAGONAL_DOWN)
            {
                window[i] = board[x + i, y + (Constants.DISCS_TO_WIN - 1) - i];
            }
            else
            {
                Debug.LogWarning("Invalid direction!");
            }
        }
    }

    int CalculateScore(ref int player)
    {
        int score = 0;

        if (Count(ref window, player) == Constants.DISCS_TO_WIN)
        {
            score += Constants.DISCS_4;
        }
        else if (Count(ref window, player) == Constants.DISCS_TO_WIN - 1
            && Count(ref window, 0) == 1)
        {
            score += Constants.DISCS_3;
        }
        else if (Count(ref window, player) == Constants.DISCS_TO_WIN - 2
            && Count(ref window, 0) == 2)
        {
            score += Constants.DISCS_2;
        }

        if (Count(ref window, -player) == Constants.DISCS_TO_WIN - 1
            && Count(ref window, 0) == 1)
        {
            score += Constants.ENEMY_DISCS_3;
        }

        return score;
    }

    int Count(ref int[] array, int value)
    {
        int sum = 0;

        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
            {
                sum++;
            }
        }

        return sum;
    }

    int ChooseRandom(List<int> list)
    {
        return (list.Count > 0) ? list[Random.Range(0, list.Count)] : -1;
    }

    int MoveCheck(int[,] board, int x)
    {
        for (int i = 0; i < Constants.Y; i++)
        {
            if (board[x, i] == 0)
            {
                return i;
            }
        }

        return Constants.MOVE_INVALID;
    }
}