using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int playerType;
    public static int playerDifficulty;
    public static int enemyType;
    public static int enemyDifficulty;
    public static int musicVolume = 30;
    public static int soundVolume = 30;
    public static string playerDifficultyString;
    public static string enemyDifficultyString;

    public bool isMenu = true;
    public Text playerTypeText;
    public Text playerDifficultyText;
    public Text enemyTypeText;
    public Text enemyDifficultyText;
    public Text musicText;
    public Text soundText;
    public SoundManager soundManager;
    public GameObject playerDifficultyBar;
    public GameObject enemyDifficultyBar;
    public Image playerIcon;
    public Image enemyIcon;
    public Sprite[] sprites;

    void Start()
    {
        if (isMenu)
        {
            soundManager = FindObjectOfType<SoundManager>().GetComponent<SoundManager>();
            musicText.text = Constants.MUSIC + musicVolume.ToString();
            soundText.text = Constants.SOUND + soundVolume.ToString();
            int playerLastDifficulty = playerDifficulty;
            int enemyLastDifficulty = enemyDifficulty;
            SelectType(false, ref playerType, playerTypeText, playerDifficultyBar);
            SelectType(true, ref enemyType, enemyTypeText, enemyDifficultyBar);
            SetDifficulty(playerLastDifficulty, false);
            SetDifficulty(enemyLastDifficulty, true);
        }
        else
        {
            SetIngameType(false, ref playerType, ref playerDifficultyString, playerTypeText);
            SetIngameType(true, ref enemyType, ref enemyDifficultyString, enemyTypeText);
        }
    }

    public void PlayGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void MusicIncrease()
    {
        musicVolume += 10;

        if (musicVolume > 100)
        {
            musicVolume = 0;
        }

        musicText.text = Constants.MUSIC + musicVolume.ToString();
        soundManager.SetMusicVolume(musicVolume);
    }

    public void SoundIncrease()
    {
        soundVolume += 10;

        if (soundVolume > 100)
        {
            soundVolume = 0;
        }

        soundText.text = Constants.SOUND + soundVolume.ToString();
        soundManager.SetSoundVolume(soundVolume);
        soundManager.PlaySound(Constants.SOUND_PUT);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void BackToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void PreviousType(bool isEnemy)
    {
        if (isEnemy)
        {
            enemyType--;
            SelectType(isEnemy, ref enemyType, enemyTypeText, enemyDifficultyBar);
        }
        else
        {
            playerType--;
            SelectType(isEnemy, ref playerType, playerTypeText, playerDifficultyBar);
        }
    }

    public void NextType(bool isEnemy)
    {
        if (isEnemy)
        {
            enemyType++;
            SelectType(isEnemy, ref enemyType, enemyTypeText, enemyDifficultyBar);
        }
        else
        {
            playerType++;
            SelectType(isEnemy, ref playerType, playerTypeText, playerDifficultyBar);
        }
    }

    void SelectType(bool isEnemy, ref int currentPlayerType, Text currentPlayerText, GameObject currentDifficultyBar)
    {
        switch (currentPlayerType)
        {
            case 0:
                currentPlayerText.text = Constants.RANDOM;
                ChangeIcon(ref isEnemy);
                currentDifficultyBar.SetActive(false);
                break;
            case 1:
                currentPlayerText.text = Constants.HUMAN;
                ChangeIcon(ref isEnemy);
                currentDifficultyBar.SetActive(false);
                break;
            case 2:
                currentPlayerText.text = Constants.MINMAX;
                ChangeIcon(ref isEnemy);
                SetDifficulty(3, isEnemy);
                currentDifficultyBar.SetActive(true);
                break;
            case 3:
                currentPlayerText.text = Constants.AI;
                ChangeIcon(ref isEnemy);
                SetDifficulty(1, isEnemy);
                currentDifficultyBar.SetActive(true);
                break;
            default:
                currentPlayerType = (currentPlayerType < 0) ? 3 : 0;
                SelectType(isEnemy, ref currentPlayerType, currentPlayerText, currentDifficultyBar);
                break;
        }
    }

    void ChangeIcon(ref bool isEnemy)
    {
        if (isEnemy)
        {
            enemyIcon.sprite = sprites[enemyType];
            enemyIcon.SetNativeSize();
        }
        else
        {
            playerIcon.sprite = sprites[playerType];
            playerIcon.SetNativeSize();
        }
    }

    public void PreviousDifficulty(bool isEnemy)
    {
        if (isEnemy)
        {
            SetDifficulty(enemyDifficulty - 1, isEnemy);
        }
        else
        {
            SetDifficulty(playerDifficulty - 1, isEnemy);
        }
    }

    public void NextDifficulty(bool isEnemy)
    {
        if (isEnemy)
        {
            SetDifficulty(enemyDifficulty + 1, isEnemy);
        }
        else
        {
            SetDifficulty(playerDifficulty + 1, isEnemy);
        }
    }

    void SetDifficulty(int difficulty, bool isEnemy)
    {
        if (isEnemy)
        {
            enemyDifficulty = difficulty;
            SelectDifficulty(ref enemyType, ref enemyDifficulty, ref enemyDifficultyString, enemyDifficultyText);
        }
        else
        {
            playerDifficulty = difficulty;
            SelectDifficulty(ref playerType, ref playerDifficulty, ref playerDifficultyString, playerDifficultyText);
        }
    }

    void SelectDifficulty(ref int currentPlayerType, ref int currentPlayerDifficulty, ref string currentDifficultyString, Text currentDifficultyText)
    {
        if (currentPlayerType == 3)
        {
            switch (currentPlayerDifficulty)
            {
                case 0:
                    currentDifficultyString = Constants.EASY;
                    break;
                case 1:
                    currentDifficultyString = Constants.MEDIUM;
                    break;
                case 2:
                    currentDifficultyString = Constants.HARD;
                    break;
                default:
                    currentPlayerDifficulty = (currentPlayerDifficulty < 0) ? 2 : 0;
                    SelectDifficulty(ref currentPlayerType, ref currentPlayerDifficulty, ref currentDifficultyString, currentDifficultyText);
                    break;
            }

            currentDifficultyText.text = currentDifficultyString;
        }
        else
        {
            if (currentPlayerDifficulty < 1)
            {
                currentPlayerDifficulty = 8;
            }
            else if (currentPlayerDifficulty > 8)
            {
                currentPlayerDifficulty = 1;
            }

            currentDifficultyString = currentPlayerDifficulty.ToString();
            currentDifficultyText.text = Constants.DEPTH + " " + currentDifficultyString;
        }
    }

    void SetIngameType(bool isEnemy, ref int currentPlayerType, ref string currentDifficultyString, Text currentPlayerText)
    {
        switch (currentPlayerType)
        {
            case 0:
                currentPlayerText.text = Constants.RANDOM;
                break;
            case 1:
                currentPlayerText.text = Constants.HUMAN;
                break;
            case 2:
                currentPlayerText.text = Constants.MINMAX + " " + currentDifficultyString;
                break;
            case 3:
                currentPlayerText.text = Constants.AI + " " + currentDifficultyString;
                break;
        }

        ChangeIcon(ref isEnemy);
    }
}
