using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public int turnState = Constants.PLAYER_1;
    public int turnCount = 0;
    public int action = Constants.MOVE_INVALID;
    public bool winPause = false;
    public bool fallingAnimation = false;
    public bool falling = false;
    public float fallingAcceleration = 30f;
    public int[,] board = new int[Constants.X, Constants.Y];
    public Sprite enemySprite;
    public UnityEngine.UI.Text drawScoreText;
    public UnityEngine.UI.Text playerScoreText;
    public UnityEngine.UI.Text enemyScoreText;
    public UnityEngine.UI.Text winText;
    public AIAgent playerAgent;
    public AIAgent enemyAgent;
    public AIAgent currentAgent;
    public UserInput move;
    public ColorManager colorManager;
    public MinMax minMax;
    public TestManager testManager;
    public GameObject disc;
    public List<GameObject> discs;
    public List<Unity.Barracuda.NNModel> models;

    private int playerScore = 0;
    private int enemyScore = 0;
    private int drawScore = 0;
    private StatsRecorder stats;
    private SoundManager soundManager;

    void Start()
    {
        switch (GameManager.playerType)
        {
            case 1:
                playerAgent.playerControl = true;
                break;
            case 2:
                playerAgent.depth = GameManager.playerDifficulty;
                playerAgent.minMaxControl = true;
                break;
            case 3:
                playerAgent.GetComponent<Unity.MLAgents.Policies.BehaviorParameters>().Model = models[GameManager.playerDifficulty];
                break;
        }

        switch (GameManager.enemyType)
        {
            case 1:
                enemyAgent.playerControl = true;
                break;
            case 2:
                enemyAgent.depth = GameManager.enemyDifficulty;
                enemyAgent.minMaxControl = true;
                break;
            case 3:
                enemyAgent.GetComponent<Unity.MLAgents.Policies.BehaviorParameters>().Model = models[GameManager.enemyDifficulty];
                break;
        }

        soundManager = FindObjectOfType<SoundManager>().GetComponent<SoundManager>();

        if (!playerAgent.playerControl && !enemyAgent.playerControl)
        {
            move.enabled = false;
        }

        // Academy preparation
        Academy.Instance.AutomaticSteppingEnabled = false;
        stats = Academy.Instance.StatsRecorder;

        // Start the game
        playerAgent.RequestDecision();
        Academy.Instance.EnvironmentStep();
    }

    void FixedUpdate()
    {
        currentAgent = (turnState == Constants.PLAYER_1) ? playerAgent : enemyAgent;

        if (playerAgent.playerControl && currentAgent == playerAgent)
        {
            enabled = false;
            StartCoroutine(WaitForKeyDown());
        }
        else if (enemyAgent.playerControl && currentAgent == enemyAgent)
        {
            enabled = false;
            StartCoroutine(WaitForKeyDown());
        }
        else
        {
            enabled = false;
            StartCoroutine(GameStep(false));
        }
    }

    IEnumerator WaitForKeyDown()
    {
        while
            (
                !Input.GetKeyDown(KeyCode.Alpha1) &&
                !Input.GetKeyDown(KeyCode.Alpha2) &&
                !Input.GetKeyDown(KeyCode.Alpha3) &&
                !Input.GetKeyDown(KeyCode.Alpha4) &&
                !Input.GetKeyDown(KeyCode.Alpha5) &&
                !Input.GetKeyDown(KeyCode.Alpha6) &&
                !Input.GetKeyDown(KeyCode.Alpha7) &&
                !(Input.GetMouseButtonDown(Constants.LEFT_CLICK) &&
                move.validMove)
            )
        {
            yield return null;
        }

        StartCoroutine(GameStep(true));
    }

    IEnumerator GameStep(bool isHuman)
    {
        // Agent's decision
        currentAgent.RequestDecision();
        Academy.Instance.EnvironmentStep();
        
        int y = MoveCheck(action);
        if (y >= 0)
        {
            Move(action, y, turnState);
            yield return new WaitWhile(() => falling);

            // Switch the current agent;
            currentAgent = null;
            turnState = -turnState;

            GameOverChecker(-turnState);
        }
        else
        {
            if (isHuman)
            {
                Debug.LogWarning("Invalid player move!");
            }
            else
            {
                Debug.LogWarning("Invalid agent move!");
            }
            enabled = true;
        }
    }

    public int MoveCheck(int x)
    {
        for (int i = 0; i < Constants.Y; i++)
        {
            if (board[x, i] == 0)
            {
                return i;
            }
        }

        return Constants.MOVE_INVALID; 
    }

    void Move(int x, int y, int type)
    {
        board[x, y] = type;
        AddDisc(x, y, type);
        turnCount++;
    }

    void AddDisc(int x, int y, int type)
    {
        if (fallingAnimation)
        {
            Vector3 startPosition = new Vector3(transform.position.x + x, transform.position.y + Constants.Y - 1);
            Vector3 endPosition = new Vector3(transform.position.x + x, transform.position.y + y);
            GameObject newDisc = Instantiate(disc, startPosition, Quaternion.identity, this.transform);
            discs.Add(newDisc);

            if (type == Constants.PLAYER_2)
            {
                newDisc.GetComponent<SpriteRenderer>().sprite = enemySprite;
            }
            
            falling = true;
            StartCoroutine(FallingTransition(newDisc, endPosition));
        }
        else
        {
            Vector3 position = new Vector3(transform.position.x + x, transform.position.y + y);
            GameObject newDisc = Instantiate(disc, position, Quaternion.identity, this.transform);
            discs.Add(newDisc);

            if (type == Constants.PLAYER_2)
            {
                newDisc.GetComponent<SpriteRenderer>().sprite = enemySprite;
            }
        }
    }

    IEnumerator FallingTransition(GameObject disc, Vector3 endPosition)
    {
        float speed = 0f;

        while (disc.transform.position != endPosition)
        {
            disc.transform.position = Vector3.MoveTowards(disc.transform.position, endPosition, speed * Time.deltaTime);
            speed += fallingAcceleration * Time.deltaTime;
            yield return null;
        }
        
        soundManager.PlaySound(Constants.SOUND_DROP);
        falling = false;
    }

    void GameOverChecker(int type)
    {
        if (turnCount >= Constants.MAX_SLOTS)
        {
            drawScore++;
            drawScoreText.text = drawScore.ToString();
            winText.text = Constants.DRAW_TEXT;
            soundManager.PlaySound(Constants.SOUND_PUT);
            TrackScore(drawScore, Constants.NO_WINNER);

            if (winPause)
            {
                winText.enabled = true;
                StartCoroutine(ResetAfter(Constants.NO_WINNER, Constants.WAIT_TIME));
            }
            else
            {
                Reset(Constants.NO_WINNER);
            }
        }
        else if (IsGameOver() == type)
        {
            if (type == Constants.PLAYER_1)
            {
                playerScore++;
                playerScoreText.text = playerScore.ToString();
                winText.text = Constants.P1_WINS;
                soundManager.PlaySound(Constants.SOUND_WIN);
                TrackScore(playerScore, type);
            }
            else
            {
                enemyScore++;
                enemyScoreText.text = enemyScore.ToString();
                winText.text = Constants.P2_WINS;
                soundManager.PlaySound(Constants.SOUND_LOSS);
                TrackScore(enemyScore, type);
            }

            if (winPause)
            {
                winText.enabled = true;
                StartCoroutine(ResetAfter(type, Constants.WAIT_TIME));
            }
            else
            {
                Reset(type);
            }
        }
        else
        {
            enabled = true;
        }
    }

    void TrackScore(int score, int type)
    {
        if (type == Constants.PLAYER_1)
        {
            stats.Add("Scores/Player 1", score);
        }
        else if (type == Constants.PLAYER_2)
        {
            stats.Add("Scores/Player 2", score);
        }
        else
        {
            stats.Add("Scores/Draw", score);
        }

        stats.Add("Scores/Score difference", playerScore - enemyScore);

        if (playerScore > 0)
        {
            stats.Add("Scores/Score % diff", (float)(playerScore - enemyScore) / playerScore * 100);
        }
    }

    void Reset(int winner)
    {
        if (winner == Constants.PLAYER_1)
        {
            playerAgent.SetReward(Constants.WIN);
            enemyAgent.SetReward(Constants.LOSS);
        }
        else if (winner == Constants.PLAYER_2)
        {
            playerAgent.SetReward(Constants.LOSS);
            enemyAgent.SetReward(Constants.WIN);
        }
        else
        {
            playerAgent.SetReward(Constants.DRAW);
            enemyAgent.SetReward(Constants.DRAW);
        }

        playerAgent.EndEpisode();
        enemyAgent.EndEpisode();

        ClearBoard();

        #if UNITY_EDITOR
            if (testManager)
            {
                testManager.roundCount++;
                testManager.Test(ref playerScore, ref enemyScore, ref drawScore);
            }
        #endif
        
        winText.enabled = false;
        turnCount = 0;
        turnState = Constants.PLAYER_1;
        enabled = true;
    }

    void ClearBoard()
    {
        foreach (GameObject item in discs)
        {
            Destroy(item);
        }
        discs.Clear();
        colorManager.coloredDiscs.Clear();
        System.Array.Clear(board, 0, board.Length);
    }

    int IsGameOver()
    {
        int check1 = CheckHorizontally();
        int check2 = CheckVertically();
        int check3 = CheckDiagonally();

        if (check1 != 0) return check1;
        if (check2 != 0) return check2;
        return check3;
    }

    int CheckHorizontally()
    {
        int sum = 0;

        for (int j = 0; j < Constants.Y; j++)
        {
            for (int i = 0; i < Constants.X; i++)
            {
                Addition(ref i, ref j, ref sum);
                
                if (sum >= Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorHorizontally(i, j, Constants.PLAYER_1);
                    return Constants.PLAYER_1;
                }
                else if (sum <= -Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorHorizontally(i, j, Constants.PLAYER_2);
                    return Constants.PLAYER_2;
                }
            }
            sum = 0;
        }

        return Constants.NO_WINNER;
    }

    int CheckVertically()
    {
        int sum = 0;

        for (int i = 0; i < Constants.X; i++)
        {
            for (int j = 0; j < Constants.Y; j++)
            {
                Addition(ref i, ref j, ref sum);

                if (sum >= Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorVertically(i, j, Constants.PLAYER_1);
                    return Constants.PLAYER_1;
                }
                else if (sum <= -Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorVertically(i, j, Constants.PLAYER_2);
                    return Constants.PLAYER_2;
                }
            }
            sum = 0;
        }

        return Constants.NO_WINNER;
    }

    int CheckDiagonally()
    {
        int sumUp = 0;
        int sumDown = 0;
        for (int j = 0; j < Constants.X + Constants.Y - 1; j++)
        {
            for (int i = 0; i < Constants.X; i++)
            {
                int yUp = Constants.Y - 1 - j + i;
                int yDown = j - i;

                if (yUp > Constants.Y - 1 || yUp < 0 || yDown > Constants.Y - 1 || yDown < 0)
                {
                    continue;
                }

                Addition(ref i, ref yUp, ref sumUp);
                Addition(ref i, ref yDown, ref sumDown);
                
                if (sumUp >= Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorDiagonally(Constants.PLAYER_1, true);
                    return Constants.PLAYER_1;
                }
                else if (sumDown >= Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorDiagonally(Constants.PLAYER_1, false);
                    return Constants.PLAYER_1;
                }
                else if (sumUp <= -Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorDiagonally(Constants.PLAYER_2, true);
                    return Constants.PLAYER_2;
                }
                else if (sumDown <= -Constants.DISCS_TO_WIN)
                {
                    colorManager.ColorDiagonally(Constants.PLAYER_2, false);
                    return Constants.PLAYER_2;
                }
            }
            sumUp = 0;
            sumDown = 0;
        }

        return Constants.NO_WINNER;
    }
    
    public void Addition(ref int i, ref int j, ref int sum)
    {
        if (board[i, j] == 0)
        {
            sum = 0;
        }
        else if (board[i, j] < 0 && sum > 0)
        {
            sum = 0;
            sum += board[i, j];
        }
        else if (board[i, j] > 0 && sum < 0)
        {
            sum = 0;
            sum += board[i, j];
        }
        else
        {
            sum += board[i, j];
        }
    }

    IEnumerator ResetAfter(int winner, int seconds)
    {
        yield return new WaitForSeconds(seconds);

        Reset(winner);
    }

    void EnemyMoveRandom()
    {
        int x = Random.Range(0, Constants.X);
        int y = MoveCheck(x);
        if (y >= 0)
        {
            board[x, y] = Constants.PLAYER_2;
            AddDisc(x, y, Constants.PLAYER_2);
            turnCount++;
        }
        else
        {
            EnemyMoveRandom();
        }
    }
    
    public GameObject FindDisc(int x, int y)
    {
        foreach (GameObject item in discs)
        {
            if (item.transform.position.x == x && item.transform.position.y == y)
            {
                return item;
            }
        }

        return null;
    }
}
