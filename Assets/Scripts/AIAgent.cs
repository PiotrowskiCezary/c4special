using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class AIAgent : Agent
{
    public BoardManager boardManager;
    public bool maskEnabled = true;
    public bool playerControl = false;
    public bool minMaxControl = false;
    public int decision = Constants.MOVE_INVALID;
    public int depth = 3;

    public override void WriteDiscreteActionMask(IDiscreteActionMask actionMask)
    {
        if (maskEnabled)
        {
            for (int i = 0; i < Constants.X; i++)
            {
                if (boardManager.MoveCheck(i) <= Constants.MOVE_INVALID)
                {
                    actionMask.SetActionEnabled(Constants.BRANCH, i, false);
                }
            }
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Feed the agent with information about the state of the board
        for (int j = 0; j < Constants.Y; j++)
        {
            for (int i = 0; i < Constants.X; i++)
            {
                sensor.AddObservation(boardManager.board[i, j]);
            }
        }

        // And information about whose turn it is now
        sensor.AddObservation(boardManager.turnState);
    }

    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        decision = actionBuffers.DiscreteActions[0];
        switch (decision)
        {
            case Constants.MOVE_0:
                boardManager.action = decision;
                break;
            case Constants.MOVE_1:
                boardManager.action = decision;
                break;
            case Constants.MOVE_2:
                boardManager.action = decision;
                break;
            case Constants.MOVE_3:
                boardManager.action = decision;
                break;
            case Constants.MOVE_4:
                boardManager.action = decision;
                break;
            case Constants.MOVE_5:
                boardManager.action = decision;
                break;
            case Constants.MOVE_6:
                boardManager.action = decision;
                break;
            default:
                Debug.Log("Invalid action value!");
                break;
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var discreteActionsOut = actionsOut.DiscreteActions;

        if (playerControl)
        {
            discreteActionsOut[0] = boardManager.move.last;
        }
        else if (minMaxControl)
        {
            discreteActionsOut[0] = boardManager.minMax.Algorithm(boardManager.board, depth, int.MinValue, int.MaxValue, boardManager.turnState, true)[0];
        }
        else
        {
            int x, y;

            do
            {
                x = Random.Range(0, Constants.X);
                y = boardManager.MoveCheck(x);
            } while (y <= Constants.MOVE_INVALID);

            discreteActionsOut[0] = x;
        }
    }
}
