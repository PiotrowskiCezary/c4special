using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    public Unity.MLAgents.Policies.BehaviorParameters behParams;
    
    public List<Unity.Barracuda.NNModel> models;
    public int modelIndex = 0;
    public int roundCount = 0;
    public List<string> results;

    void Start()
    {
        behParams.Model = models[modelIndex];
    }

    public void Test(ref int playerScore, ref int enemyScore, ref int drawScore)
    {
        if (roundCount >= Constants.TEST_ROUNDS)
        {
            float win = ((float)playerScore / roundCount) * 100;
            float loss = ((float)enemyScore / roundCount) * 100;
            float draw = ((float)drawScore / roundCount) * 100;
            string text = $"[{modelIndex + 1}] P1: {win.ToString("F1")}%\tP2: {loss.ToString("F1")}%\tDraw: {draw.ToString("F1")}%";
            results.Add(text);
            playerScore = 0;
            enemyScore = 0;
            drawScore = 0;
            NextTest();
        }
    }

    void NextTest()
    {
        modelIndex++;

        if (modelIndex >= models.Count)
        {
            StringBuilder text = new StringBuilder();
            foreach (string result in results)
            {
                text.AppendLine(result);
            }
            modelIndex = 0;
            Debug.Log(text);
            Debug.Break();
        }

        roundCount = 0;
        behParams.Model = models[modelIndex];
    }
}
