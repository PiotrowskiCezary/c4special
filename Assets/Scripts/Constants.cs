public class Constants
{
    // Common
    public const int X = 7;
    public const int Y = 6;
    public const int PLAYER_1 = 1;
    public const int PLAYER_2 = -1;
    public const int DISCS_TO_WIN = 4;
    public const int MOVE_INVALID = -1;
    public const int MOVE_0 = 0;
    public const int MOVE_1 = 1;
    public const int MOVE_2 = 2;
    public const int MOVE_3 = 3;
    public const int MOVE_4 = 4;
    public const int MOVE_5 = 5;
    public const int MOVE_6 = 6;
    public const int LEFT_CLICK = 0;

    // BoardManager
    public const int MAX_SLOTS = X * Y;
    public const int NO_WINNER = 0;
    public const int WAIT_TIME = 4;
    public const int SOUND_DROP = 0;
    public const int SOUND_LOSS = 1;
    public const int SOUND_WIN = 2;
    public const float WIN = 1f;
    public const float DRAW = 0f;
    public const float LOSS = -1f;
    public const string P1_WINS = "P1 Wins!";
    public const string P2_WINS = "P2 Wins!";
    public const string DRAW_TEXT = "Draw!";

    // AIAgent
    public const int BRANCH = 0;

    // ColorManager
    public const int RED = 4;
    public const int GREEN = 2;
    public const int BLUE = 3;

    // MinMax
    public const int HORIZONTAL = 1;
    public const int VERTICAL = 2;
    public const int DIAGONAL_UP = 3;
    public const int DIAGONAL_DOWN = 4;
    public const int DISCS_CENTER = 3;
    public const int DISCS_4 = 100;
    public const int DISCS_3 = 5;
    public const int DISCS_2 = 2;
    public const int ENEMY_DISCS_3 = -4;

    // TestManager
    public const int TEST_ROUNDS = 1000;

    // GameManager
    public const int SOUND_PUT = 3;
    public const string MUSIC = "Music: ";
    public const string SOUND = "Sound: ";
    public const string RANDOM = "Random";
    public const string HUMAN = "Human";
    public const string MINMAX = "MinMax";
    public const string AI = "AI";
    public const string EASY = "Easy";
    public const string MEDIUM = "Medium";
    public const string HARD = "Hard";
    public const string DEPTH = "Depth";
}