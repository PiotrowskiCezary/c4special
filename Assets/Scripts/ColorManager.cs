using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public bool debug = false;
    public bool colorize = false;
    public BoardManager boardManager;
    public List<GameObject> coloredDiscs;

    public void ColorHorizontally(int x, int y, int type)
    {
        if (colorize)
        {
            int sum = 0;
            int last = 0;

            for (int i = x - (Constants.DISCS_TO_WIN - 1); i < Constants.X; i++)
            {
                sum += boardManager.board[i, y];
                if (type == Constants.PLAYER_1)
                {
                    if (sum <= last)
                    {
                        break;
                    }
                }
                else
                {
                    if (sum >= last)
                    {
                        break;
                    }
                }
                coloredDiscs.Add(boardManager.FindDisc(i, y));
                last = sum;
            }

            int color = (debug) ? Constants.GREEN : type;
            ColorDiscs(color);
        }
    }
    
    public void ColorVertically(int x, int y, int type)
    {
        if (colorize)
        {
            int sum = 0;
            int last = 0;

            for (int i = y - (Constants.DISCS_TO_WIN - 1); i < Constants.Y; i++)
            {
                sum += boardManager.board[x, i];
                if (type == Constants.PLAYER_1)
                {
                    if (sum <= last)
                    {
                        break;
                    }
                }
                else
                {
                    if (sum >= last)
                    {
                        break;
                    }
                }
                coloredDiscs.Add(boardManager.FindDisc(x, i));
                last = sum;
            }

            int color = (debug) ? Constants.BLUE : type;
            ColorDiscs(color);
        }
    }

    public void ColorDiagonally(int type, bool isUp)
    {
        if (colorize)
        {
            int sumUp = 0;
            int sumDown = 0;

            // Search up
            for (int j = 0; j < Constants.X + Constants.Y - 1; j++)
            {
                for (int i = 0; i < Constants.X; i++)
                {
                    int yUp = Constants.Y - 1 - j + i;

                    if (yUp <= Constants.Y - 1 && yUp >= 0)
                    {
                        boardManager.Addition(ref i, ref yUp, ref sumUp);

                        if (sumUp >= Constants.DISCS_TO_WIN || sumUp <= -Constants.DISCS_TO_WIN)
                        {
                            ColorRecursive(i, yUp, type, 0);
                            ColorRecursive(i, yUp, type, 2);
                        }
                    }
                }
                sumUp = 0;
            }

            // Search down
            for (int j = 0; j < Constants.X + Constants.Y - 1; j++)
            {
                for (int i = 0; i < Constants.X; i++)
                {
                    int yDown = j - i;
                    if (yDown <= Constants.Y - 1 && yDown >= 0)
                    {
                        boardManager.Addition(ref i, ref yDown, ref sumDown);

                        if (sumDown >= Constants.DISCS_TO_WIN || sumDown <= -Constants.DISCS_TO_WIN)
                        {
                            ColorRecursive(i, yDown, type, 1);
                            ColorRecursive(i, yDown, type, 3);
                        }
                    }
                }
                sumDown = 0;
            }

            int color = (debug) ? Constants.RED : type;
            ColorDiscs(color);
        }
    }

    void ColorRecursive(int x, int y, int type, int dir)
    {
        int stepX = 0;
        int stepY = 0;

        switch (dir)
        {
            case 0:
                stepX = -1;
                stepY = -1;
                break;
            case 1:
                stepX = 1;
                stepY = -1;
                break;
            case 2:
                stepX = 1;
                stepY = 1;
                break;
            case 3:
                stepX = -1;
                stepY = 1;
                break;
            default:
                break;
        }

        if (x >= 0 && x < Constants.X && y >= 0 && y < Constants.Y)
        {
            if (boardManager.board[x, y] == type)
            {
                coloredDiscs.Add(boardManager.FindDisc(x, y));
                ColorRecursive(x + stepX, y + stepY, type, dir);
            }
        }
    }

    void ColorDiscs(int type)
    {
        Color newColor;

        switch (type)
        {
            case Constants.PLAYER_1:
                newColor = Color.green;
                break;
            case Constants.GREEN:
                newColor = Color.green;
                break;
            case Constants.BLUE:
                newColor = Color.blue;
                break;
            case Constants.RED:
                newColor = Color.red;
                break;
            default:
                newColor = Color.red;
                break;
        }

        foreach (GameObject item in coloredDiscs)
        {
            item.GetComponent<SpriteRenderer>().color = newColor;
        }

        if (debug)
        {
            coloredDiscs.Clear();
        }
    }
}
