using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance = null;

    public AudioSource musicSource;
    public AudioSource soundSource;
    public AudioClip[] sounds;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void SetMusicVolume(int volume)
    {
        musicSource.volume = (float)volume / 100;
    }

    public void SetSoundVolume(int volume)
    {
        soundSource.volume = (float)volume / 100;
    }

    public void PlaySound(int soundIndex)
    {
        soundSource.clip = sounds[soundIndex];
        soundSource.Play();
    }
}