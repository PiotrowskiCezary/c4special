using UnityEngine;

public class UserInput : MonoBehaviour
{
    public int mouseX = Constants.MOVE_INVALID;
    public int input = Constants.MOVE_INVALID;
    public int last = Constants.MOVE_INVALID;
    public bool validMove = false;
    public GameObject discPreview;
    public BoardManager boardManager;

    private Sprite playerSprite;

    void Start()
    {
        playerSprite = discPreview.GetComponent<SpriteRenderer>().sprite;
    }

    void Update()
    {
        validMove = false;
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseX = (int)Mathf.Round(mouseWorldPosition.x);
        MousePress();
        KeyPress();
        MouseHover();
    }
    
    void MousePress()
    {
        if (Input.GetMouseButton(Constants.LEFT_CLICK))
        {
            switch (mouseX)
            {
                case Constants.MOVE_0:
                    input = Constants.MOVE_0;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_1:
                    input = Constants.MOVE_1;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_2:
                    input = Constants.MOVE_2;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_3:
                    input = Constants.MOVE_3;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_4:
                    input = Constants.MOVE_4;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_5:
                    input = Constants.MOVE_5;
                    last = input;
                    validMove = true;
                    break;
                case Constants.MOVE_6:
                    input = Constants.MOVE_6;
                    last = input;
                    validMove = true;
                    break;
                default:
                    input = Constants.MOVE_INVALID;
                    validMove = false;
                    break;
            }
        }
    }

    void KeyPress()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            input = Constants.MOVE_0;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            input = Constants.MOVE_1;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha3))
        {
            input = Constants.MOVE_2;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha4))
        {
            input = Constants.MOVE_3;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha5))
        {
            input = Constants.MOVE_4;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha6))
        {
            input = Constants.MOVE_5;
            last = input;
        }
        else if (Input.GetKey(KeyCode.Alpha7))
        {
            input = Constants.MOVE_6;
            last = input;
        }
        else
        {
            input = Constants.MOVE_INVALID;
        }
    }
    
    void MouseHover()
    {
        if (!boardManager.falling)
        {
            if (boardManager.turnState == Constants.PLAYER_1)
            {
                discPreview.GetComponent<SpriteRenderer>().sprite = playerSprite;
            }
            else
            {
                discPreview.GetComponent<SpriteRenderer>().sprite = boardManager.enemySprite;
            }

            if (boardManager.currentAgent)
            {
                if (boardManager.currentAgent.playerControl)
                {
                    if (mouseX >= Constants.MOVE_0 && mouseX <= Constants.MOVE_6)
                    {
                        discPreview.SetActive(true);
                        discPreview.transform.position = new Vector3(mouseX, Constants.Y - 1);
                    }
                    else
                    {
                        discPreview.SetActive(false);
                    }
                }
            }
        }
        else
        {
            discPreview.SetActive(false);
        }
    }
}
